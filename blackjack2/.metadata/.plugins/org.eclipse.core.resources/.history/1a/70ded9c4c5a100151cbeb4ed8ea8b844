package proyecto.cei.server;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import dominio.BlackjackGame;
import dominio.JugadorImpl;
import proyecto.cei.comun.BlackjackServer;
import proyecto.cei.comun.Jugador;
import proyecto.cei.comun.ObservadorServidor;
import proyecto.cei.comun.ObserverCliente;

public class BlackjackServerImpl implements BlackjackServer, ObservadorServidor {

	private Map<String,ObserverCliente> observers = new HashMap<String,ObserverCliente>();
	private BlackjackGame game = new BlackjackGame();
	private static BlackjackServerImpl instance;
	
	public BlackjackServerImpl() {
		
		game.setObservadorServidor(this);
		
	}
	
	public static BlackjackServerImpl getInstance(){
		if(instance == null){
			instance = new BlackjackServerImpl();
		}
		
		return instance;
		
	}
	
	public Jugador conectar(String nombre, ObserverCliente observer) throws RemoteException {
		Jugador nuevo = new JugadorImpl(nombre);
		nuevo.setId(nuevo.getNombre() + "_" + observers.size());
		observers.put(nuevo.getId(),observer);
		game.agregarJugador(nuevo);
		
		return nuevo;
	}

	public void notificarNuevoJugador(Jugador j) throws RemoteException{
		java.util.Iterator<Entry<String, ObserverCliente>> it = observers.entrySet().iterator();
		
		while(it.hasNext()){
			
			ObserverCliente actual = it.next().getValue();
			actual.notificarNuevoJugador(j);
			
		}
	}

	public void notificarCartaNueva(Jugador j) throws RemoteException {
		
		java.util.Iterator<Entry<String, ObserverCliente>> it = observers.entrySet().iterator();
		
		while(it.hasNext()){
			
			ObserverCliente actual = it.next().getValue();
			actual.notificarCartaNueva(j);
			
		}
	}

	public void notificarTurnoJugador(Jugador jugadorTurno) throws RemoteException {
		
		java.util.Iterator<Entry<String, ObserverCliente>> it = observers.entrySet().iterator();
		
		while(it.hasNext()){
			
			ObserverCliente actual = it.next().getValue();
			actual.notificarTurnoJugador(jugadorTurno);
			
		}
	}

	public void accionJugador(String accion, String id) throws RemoteException {
		
		game.accionJugador(accion,id);
		
	}

	public void notificarResultadoMano(Jugador j) throws RemoteException {
		
		java.util.Iterator<Entry<String, ObserverCliente>> it = observers.entrySet().iterator();
				
				while(it.hasNext()){
					
					ObserverCliente actual = it.next().getValue();
					actual.notificarResultadoMano(j);
					
				}
	}

	public void notificarResultadoManoBlackjack(Jugador j) throws RemoteException {
		
		java.util.Iterator<Entry<String, ObserverCliente>> it = observers.entrySet().iterator();
		
		while(it.hasNext()){
			
			ObserverCliente actual = it.next().getValue();
			actual.notificarResultadoManoBlackjack(j);
			
		}
		
	}

	public void notificarManoCrupier(Jugador crupier) throws RemoteException {
		
		java.util.Iterator<Entry<String, ObserverCliente>> it = observers.entrySet().iterator();
				
				while(it.hasNext()){
					
					ObserverCliente actual = it.next().getValue();
					actual.notificarManoCrupier(crupier);
					
				}
		
	}

	public void notificarRealizarApuesta() throws RemoteException {
		
		java.util.Iterator<Entry<String, ObserverCliente>> it = observers.entrySet().iterator();
		
		while(it.hasNext()){
			
			ObserverCliente actual = it.next().getValue();
			actual.notificarRealizarApuesta();
			
		}
	}

	public void notificarBlackjack(Jugador j) throws RemoteException {
		
		java.util.Iterator<Entry<String, ObserverCliente>> it = observers.entrySet().iterator();
				
				while(it.hasNext()){
					
					ObserverCliente actual = it.next().getValue();
					actual.notificarBlackjack(j);
					
				}
	}


	public void notificarJugadorSaldo() throws RemoteException {
		
		java.util.Iterator<Entry<String, ObserverCliente>> it = observers.entrySet().iterator();
		
		while(it.hasNext()){
			
			ObserverCliente actual = it.next().getValue();
			actual.notificarJugadorSaldo();
			
		}
	}

	public void notificarFinDeJuego() throws RemoteException {
		
		java.util.Iterator<Entry<String, ObserverCliente>> it = observers.entrySet().iterator();
		
		while(it.hasNext()){
			
			ObserverCliente actual = it.next().getValue();
			actual.notificarFinDeJuego();
		}
	}

	public void setApuesta(Jugador j, int apuesta) throws RemoteException {
	
		game.setApuestaJugador(j,apuesta);
	
	}

	public void actualizarSaldo(Jugador j, int saldo) throws RemoteException {
	
		java.util.Iterator<Entry<String, ObserverCliente>> it = observers.entrySet().iterator();
		
		while(it.hasNext()){
			
			ObserverCliente actual = it.next().getValue();
			actual.actualizarSaldoJugador(j,saldo);
		}
		
	}

	public int sumaMano(Jugador jugadorTurno) throws RemoteException {
		
		return game.puntajeMano( jugadorTurno.getMano() );
		
	}


}


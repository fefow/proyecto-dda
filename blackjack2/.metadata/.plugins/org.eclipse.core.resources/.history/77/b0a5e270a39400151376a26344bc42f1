package dominio;

import java.rmi.RemoteException;
import java.util.LinkedList;

import proyecto.cei.comun.Carta;
import proyecto.cei.comun.Jugador;
import proyecto.cei.comun.ObservadorServidor;
import proyecto.cei.comun.ResultadoMano;

public class BlackjackGame {

	int minimoJugadoresEmpezar = 1;
	
	LinkedList<Jugador> jugadores = new LinkedList<Jugador>();
	
	private ObservadorServidor obS;
	
	private Mazo mazo;
	
	private Jugador crupier = new JugadorImpl("Crupier");
	
	Jugador jugadorTurno;
	
	int indiceTurno = -1;

	
	public void agregarJugador(Jugador j) throws RemoteException {
		
		jugadores.add(j);
		
		ResultadoMano newResultado = new ResultadoManoImpl();
		
		j.setResultado(newResultado);

		obS.notificarNuevoJugador(j);

		if ( jugadores.size() >= minimoJugadoresEmpezar ) {
		
			iniciarJuego();
		
		}

	}

	public void setObservadorServidor(ObservadorServidor obS) {
		
		this.obS = obS;
	
	}

	private void iniciarJuego() throws RemoteException {

		obS.notificarRealizarApuesta();
		
		crupier.getMano().clear();

		indiceTurno = -1;

		mazo = new Mazo();

		mazo.barajar();

		for (Jugador j : jugadores) {

			j.getMano().clear();

			this.darCartasJugador(j, 2);

			// Pregunta si es blackjack y setea el el resultadoBlackjack en true
			
			if (puntajeMano(j.getMano()) == 21 && j.getMano().size() == 2) {
			
				j.resultado().setEsBlackjack(true);
			
			}
		}

		this.darCartasJugador(crupier, 1);

		proximoTurno();
		
	}

	private void proximoTurno() throws RemoteException {
		
		indiceTurno++;

		if ( indiceTurno < jugadores.size() ) {

			jugadorTurno = jugadores.get( indiceTurno );

			if (jugadorTurno.getSaldo() > 0) {

				obS.notificarTurnoJugador(jugadorTurno);

			} else if (jugadorTurno.getSaldo() <= 0) {
				
				//obS.notificarFinDeJuego();
			
			}

		} else {
			
			juegaCrupier();
			
			resolverJugadas();
			
			iniciarJuego();
		
		}
	}

	private void resolverJugadas() throws RemoteException {
		
		// para todos los jjugadores compara el resultado contra el crupier
		// si es mayor y no se pasaron les paga o les dice que ganaron
		// si no levanta apuesta
		// inicia el juego

		for (Jugador j : jugadores) {
			
			if ( puntajeMano( j.getMano() ) == 21 && j.getMano().size() == 2 ) {
				
				j.resultado().setResultado(1);
				
				j.resultado().setEsBlackjack(true);
				
				obS.notificarBlackjack(j);

			} else {

				if ( puntajeMano( j.getMano() ) <= 21 && puntajeMano( crupier.getMano() ) < puntajeMano( j.getMano() )) {

					j.resultado().setResultado(1);

				} else if (puntajeMano(crupier.getMano()) > puntajeMano(j.getMano())
						&& puntajeMano(crupier.getMano()) <= 21) {

					j.resultado().setResultado(-1);

				} else if (puntajeMano(crupier.getMano()) == puntajeMano(j.getMano())) {

					j.resultado().setResultado(0);
				}

			}

			obS.notificarResultadoMano(j);
			
		}

	}

	private void juegaCrupier() throws RemoteException {
		
		// mientras resultadaJugadaCrupier < 17 pide carta;

		while ( puntajeMano( crupier.getMano() ) <= 16) {
			
			darCartasJugador(crupier, 1);

			if ( puntajeMano( crupier.getMano() ) == 21 && crupier.getMano().size() == 2 ) {
			
				obS.notificarBlackjack(crupier);
			
			}
		}

		if ( puntajeMano( crupier.getMano() ) > 21 ) {
			
			obS.notificarManoCrupier(crupier);

			for (Jugador j : jugadores) {
				
				if ( puntajeMano( j.getMano() ) <= 21 ) {
					
					j.resultado().setResultado(1);
				
				}
				
			}
		}

	}

	public void darCartasJugador(Jugador j, int numCartas) throws RemoteException {
		
		if ( compararJugada( j.getMano() ) == false) {
			
			for (int i = 1; i <= numCartas; i++) {
			
				j.getMano().add( mazo.darProximaCarta() );
			
			}
			
			obS.notificarCartaNueva(j);
		
		}
	}

	public boolean compararJugada(LinkedList<Carta> cartas) throws RemoteException {

		int puntaje = puntajeMano(cartas);
		
		boolean bandera = false;

		if (puntaje > 21) {
			
			bandera = true;
		
		}

		return bandera;
		
	}

	public int puntajeMano(LinkedList<Carta> cartas) throws RemoteException {
		
		// Suma la mano y retorna la suma d las cartas.
		// Tambien pregunta si hay una A y pregunta si es el valor se pasa de
		// los 21.
		// Si se pasa toma a la A como un 1.

		int puntaje = 0;
		
		boolean tieneA = false;
		
		for (Carta carta : cartas) {

			if (carta.getFigura().equals("A")) {
				
				tieneA = true;
			
			}

			puntaje += carta.getValor();

		}

		if (puntaje > 21 && tieneA == true) {
			
			puntaje -= 10;
		
		}

		return puntaje;
		
	}

	public void accionJugador(String accion, String id) throws RemoteException {
		
		if ( jugadorTurno.getId().equals( id ) ) {
			
			if ( accion.equals("pedir") ) {

				if (revisarJugada(jugadorTurno)) {

					jugadorTurno.resultado().setResultado(-1);
					
					proximoTurno();
					

				} else {

					darCartasJugador(jugadorTurno, 1);

				}

				obS.notificarTurnoJugador(jugadorTurno);

			} else if ( accion.equals("plantarse") ) {
			
				proximoTurno();
			
			}
		}
	}

	private boolean revisarJugada(Jugador jugadorTurno) throws RemoteException {
		
		// si > 21 proximo turno
		// 
		
		boolean bandera = false;

		if ( puntajeMano( jugadorTurno.getMano() ) > 21) {
			
			jugadorTurno.resultado().setResultado(-1);
			
			bandera = true;
			
			proximoTurno();
			
		}
		
		return bandera;

	}

	public void ResolverApuesta(Jugador j, int i) throws RemoteException {
		
		for (Jugador ja : jugadores) {
			
			if ( j.getId().equals( ja.getId() ) ) {
				
				if (i == 1) {
					
					ja.setSaldo( ja.getSaldo() + ja.getApuesta() * 2 );
				
				} else if (i == 0) {
					
					ja.setSaldo( ja.getSaldo() + ja.getApuesta() );
				
				}else{
					
					ja.setSaldo( ja.getSaldo() - ja.getApuesta() );
				
				}
			}
		}
		
		obS.notificarJugadorSaldo();
	
	}

	public void setJugadorApuesta(Jugador j, int valorIngresado) throws RemoteException {
		
		for (Jugador jug : jugadores) {
			
			if ( jug.getId().equals( j.getId() ) ) {
				
				jug.setApuesta(valorIngresado);
			
			}
			
		}
		
	}

}

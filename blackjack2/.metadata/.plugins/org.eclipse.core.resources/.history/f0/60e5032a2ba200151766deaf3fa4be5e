package dominio;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import proyecto.cei.comun.Carta;
import proyecto.cei.comun.Jugador;
import proyecto.cei.comun.ObservadorServidor;
import proyecto.cei.comun.ResultadoMano;

public class BlackjackGame {

	private int minimoJugadoresEmpezar;
	
	private List<Jugador> jugadores;
	
	private ObservadorServidor obS;
	
	private Mazo mazo;
	
	private Jugador crupier;
	
	private Jugador jugadorTurno;
	
	private int indiceTurno;

	public BlackjackGame() {
		
		minimoJugadoresEmpezar = 2;
		
		jugadores = new LinkedList<Jugador>();
		
		obS = null;
		
		mazo = null;
		
		crupier = new JugadorImpl("Crupier");
		
		crupier.setId("crupier");
		
		jugadorTurno = null;
		
		indiceTurno = -1;
		
	}
	
	public void agregarJugador(Jugador j) throws RemoteException {
		
		jugadores.add(j);
		
		ResultadoMano newResultado = new ResultadoManoImpl();
		
		j.setResultado(newResultado);

		obS.notificarNuevoJugador(j);
		
		notificarJugadoresAnteriores(j);

		if ( jugadores.size() == minimoJugadoresEmpezar ) {
		
			iniciarJuego();
		
		}

	}

	private void notificarJugadoresAnteriores(Jugador jugador) throws RemoteException {
		if(jugadores.size() > 1){
			List<Jugador> jugadoresAnteriores = new ArrayList<Jugador>();
			for(Jugador j : jugadores){
				if(!j.getId().equals(jugador.getId())){
					jugadoresAnteriores.add(j);
				}				
			}
			obS.notificarJugadoresAnteriors(jugador, jugadoresAnteriores);			
		}
	}

	public void setObservadorServidor(ObservadorServidor obS) {
		
		this.obS = obS;
	
	}

	private void iniciarJuego() throws RemoteException {

		
		
		if(!verificarFinDeJuego()){
		
			notificarRealizarApuesta();
			
			Thread.sleep(1000);
			
			crupier.getMano().clear();
	
			indiceTurno = -1;
	
			mazo = new Mazo();
	
			mazo.barajar();
	
			for (Jugador j : jugadores) {
				
				j.getMano().clear();
	
				this.darCartasJugador(j, 2);
	
				// Pregunta si es blackjack y setea el el resultadoBlackjack en true
				
				if (puntajeMano(j.getMano()) == 21 && j.getMano().size() == 2) {
				
					j.resultado().setEsBlackjack(true);
				
				}
			}
	
			this.darCartasJugador(crupier, 1);
	
			proximoTurno();
		 }
	}

	private void notificarRealizarApuesta() throws RemoteException {
	
		for(Jugador jugador : jugadores){
			
			obS.notificarRealizarApuesta(jugador);
			
		}
		
	}

	private void proximoTurno() throws RemoteException {
		
		indiceTurno++;

		if ( indiceTurno < jugadores.size() ) {

			jugadorTurno = jugadores.get( indiceTurno );

			if (jugadorTurno.getSaldo() > 0) {
				
				obS.notificarTurnoJugador(jugadorTurno);
				
			}else{
				
				if(verificarFinDeJuego()){
					
					obS.notificarFinDeJuego();
					
				}else{
					
					proximoTurno();
					
				}
				
			}

		} else {
			
			//llamamos las funciones de fin de juego asincronicamente
			//para que el ultimo cliente en jugar no quede bloqueado
			
			new Thread(new Runnable() {
			    public void run() {
			    	try {
			    		
						juegaCrupier();					
					
						resolverJugadas();
						
						resolverApuesta();
						
						try {
							Thread.sleep(10000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						iniciarJuego();
						
			    	} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			    }
			}).start();		
				
			
		}
	}

	private boolean verificarFinDeJuego() throws RemoteException {
		
		boolean r = false;
		
		for( Jugador j : jugadores ){
			
			if( j.getSaldo() <= 0){
				
				r = true;
				
			}else{
				
				r = false;
				
			}
			
		}
		
		return r;
		
	}

	private void resolverJugadas() throws RemoteException {
		
		// para todos los jjugadores compara el resultado contra el crupier
		// si es mayor y no se pasaron les paga o les dice que ganaron
		// si no levanta apuesta
		// inicia el juego

		for (Jugador j : jugadores) {
			
				if ( puntajeMano( j.getMano() ) <= 21 &&
						puntajeMano( j.getMano() )  > puntajeMano( crupier.getMano() )) {

					j.resultado().setResultado(1);

				}else if(puntajeMano(j.getMano()) <= 21 && puntajeMano(crupier.getMano()) >21){
				
					j.resultado().setResultado(1);
					
				}else if (puntajeMano(crupier.getMano()) > puntajeMano(j.getMano())
						&& puntajeMano(crupier.getMano()) <= 21) {

					j.resultado().setResultado(-1);

				} else if (puntajeMano(crupier.getMano()) == puntajeMano(j.getMano())) {

					j.resultado().setResultado(0);
				
				} 
				else if ( puntajeMano(crupier.getMano()) <= 21 && puntajeMano( j.getMano() ) > 21){
					
					j.resultado().setResultado(-1);
					
				}else if ( puntajeMano(crupier.getMano()) > 21 && puntajeMano( j.getMano() ) > 21){
					
					j.resultado().setResultado(0);
					
				}
				
 
			obS.notificarResultadoMano(j);
			
		}

	}

	private void juegaCrupier() throws RemoteException {
		
		
		obS.notificarTurnoJugador(crupier);
		// mientras resultadaJugadaCrupier < 17 pide carta;

		while ( puntajeMano( crupier.getMano() ) <= 16) {
			
			darCartasJugador(crupier, 1);

			try {
				//esperamos unos segundos para que los usuarios vean las cartas del crupier
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if ( puntajeMano( crupier.getMano() ) == 21 && crupier.getMano().size() == 2 ) {
				
				obS.notificarBlackjack(crupier);
				
			}
		}

		if ( compararJugada(crupier.getMano()) ) {
			
			obS.notificarManoCrupier(crupier);
			
		}

	}

	public void darCartasJugador(Jugador j, int numCartas) throws RemoteException {
		
		if ( !compararJugada( j.getMano() ) ) {
			
			for (int i = 1; i <= numCartas; i++) {
			
				j.getMano().add( mazo.darProximaCarta() );
			
			}
			
			obS.notificarCartaNueva(j);		
			
		}
	}

	public boolean compararJugada(List<Carta> cartas) throws RemoteException {

		int puntaje = puntajeMano(cartas);
		
		boolean bandera = false;

		if (puntaje > 21) {
			
			bandera = true;
		
		}

		return bandera;
		
	}

	public int puntajeMano(List<Carta> cartas) throws RemoteException {
		
		// Suma la mano y retorna la suma d las cartas.
		// Tambien pregunta si hay una A y pregunta si es el valor se pasa de
		// los 21.
		// Si se pasa toma a la A como un 1.

		int puntaje = 0;
		
		boolean tieneA = false;
		
		for (Carta carta : cartas) {

			if (carta.getFigura().equals("A")) {
				
				tieneA = true;
			
			}

			puntaje += carta.getValor();

		}

		if (puntaje > 21 && tieneA == true) {
			
			puntaje -= 10;
		
		}

		return puntaje;
		
	}

	public void accionJugador(String accion, String id) throws RemoteException {
		
		if ( jugadorTurno.getId().equals( id ) ) {
			
			if ( accion.equals("pedir") ) {

				if (compararJugada(jugadorTurno.getMano())) {
					
					proximoTurno();
					
				} else {

					darCartasJugador(jugadorTurno, 1);
					
					if (compararJugada(jugadorTurno.getMano())) {
						
						proximoTurno();
						
					}

				}

				//obS.notificarTurnoJugador(jugadorTurno);

			} else if ( accion.equals("plantarse") ) {
			
				proximoTurno();
			
			}else if ( accion.equals("doblar") ){
				
				this.jugadorTurno.setSaldo( jugadorTurno.getSaldo() - jugadorTurno.getApuesta() );
				
				darCartasJugador(jugadorTurno, 1);
				
				accionJugador("plantarse", jugadorTurno.getId());
				
			}
		}
	}

	public void resolverApuesta() throws RemoteException {
		//Resuelve el tema de los pagos.
		
		for(Jugador j : jugadores){
			
				if ( j.resultado().getResultado() == 1 ) {
					
					if( j.resultado().esBlackjack() ){
						
						j.setSaldo( j.getSaldo() + j.getApuesta() * 2 );
						
					}else{
					
						j.setSaldo( j.getSaldo() + j.getApuesta());
				
					}
					
				} else if (j.resultado().getResultado() == 0) {
					
					j.setSaldo( (j.getSaldo() + j.getApuesta()) );
				
				} else if( j.resultado().getResultado() == -1 ){
					
					j.setSaldo( (j.getSaldo() - j.getApuesta()) );
				
				}else{
					
					System.out.println("hay tremendo error si entro aca.");
				}
				
				obS.actualizarSaldo(j, j.getSaldo());	
		}
		
		
		obS.notificarJugadorSaldo();
		
	}

	public void setApuestaJugador(Jugador j, int apuesta) {
		
		for(Jugador jugador : jugadores){
			
			if( jugador.getId().equals(j.getId()) ){
				
				jugador.setApuesta(apuesta);
				
			}
		}
	}

}

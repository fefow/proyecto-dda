package proyecto.cei.cliente;

import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.ImageIcon;
    
    
public class PanelFondo extends javax.swing.JPanel {
    	
    private String texto;
	
	public PanelFondo(String texto){
    	
		this.texto = texto;
    	this.setSize(1280,860);
    		
    }
    	
	    @Override
	    public void paintComponent (Graphics g){
	    
	    	Dimension tamanio = getSize();
	    
	    	ImageIcon imagenFondo = new ImageIcon(getClass().getResource(texto));
	    
	    	g.drawImage(imagenFondo.getImage(),0,0,tamanio.width, tamanio.height, null);
	    
	    	setOpaque(false);
	    
	    	super.paintComponent(g);
	    }
}
package dominio;

import java.rmi.RemoteException;

import proyecto.cei.comun.Carta;

public class CartaImpl implements Carta {

	private static final long serialVersionUID = 1L;
	
	private int numero;
	
	private TipoCarta tipo;
	
	private int valor;

	public CartaImpl(int num, TipoCarta tipo) {
		
		this.setNumero(num);
		
		this.setTipo(tipo);

		switch (num) {
			case 1:
				
				this.setValor(11);
				
				break;
			
			case 2:
				
				this.setValor(num);
				
				break;
			
			case 3:
				
				this.setValor(num);
				
				break;
			
			case 4:
				
				this.setValor(num);
				
				break;
			
			case 5:
				
				this.setValor(num);
				
				break;
			
			case 6:
				
				this.setValor(num);
				
				break;
			
			case 7:
				
				this.setValor(num);
				
				break;
			
			case 8:
				
				this.setValor(num);
				
				break;
			
			case 9:
				
				this.setValor(num);
				
				break;
			
			case 10:
				
				this.setValor(num);
				
				break;
			
			case 11:
				
				this.setValor(10);
				
				break;
			
			case 12:
				
				this.setValor(10);
				
				break;
			
			case 13:
				
				this.setValor(10);
				
				break;
				
		}
	}

	public void setTipo(TipoCarta tipo) {
		
		this.tipo = tipo;
	
	}

	public int getNumero() {
	
		return numero;
	
	}

	public void setNumero(int numero) {
	
		this.numero = numero;
	
	}

	public String toString() {
	
		String num = "";
		
		setValor(numero);
		
		num = resolverFiguraCarta();

		return num + " de " + tipo.toString();
	
	}

	private String resolverFiguraCarta() {
		
		String num = "";
		
		switch (this.numero) {
		
			case 1:
				
				num = "A";
				
				break;
			
			case 2:
				
				num = "2";
				
				break;
			
			case 3:
				
				num = "3";
				
				break;
			
			case 4:
				
				num = "4";
				
				break;
			
			case 5:
				
				num = "5";
				
				break;
			
			case 6:
				
				num = "6";
				
				break;
			
			case 7:
				
				num = "7";
				
				break;
			
			case 8:
				
				num = "8";
				
				break;
			
			case 9:
				
				num = "9";
				
				break;
			
			case 10:
				
				num = "10";
				
				break;
			
			case 11:
				
				num = "J";
				
				break;
			
			case 12:
				
				num = "Q";
			
				break;
			
			case 13:
				
				num = "K";
				
				break;

		}
		
		return num;
	
	}

	public int getValor() {
	
		return this.valor;
	
	}

	public void setValor(int valor) {
	
		this.valor = valor;
	
	}

	public String getTipo() throws RemoteException {
	
		return tipo.toString();
	
	}

	public String getFigura() throws RemoteException {
	
		return resolverFiguraCarta();
	
	}
}

package dominio;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import proyecto.cei.comun.Carta;
import proyecto.cei.comun.Jugador;
import proyecto.cei.comun.ResultadoMano;

@Entity
@Table(name = "jugador")
public class JugadorImpl implements Jugador {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String id;

	@Column(name = "name", nullable = false)
	private String nombre;

	private List<Carta> mano;
	private ResultadoMano resultado;
	private int saldo;
	private int apuesta;
	private int cantidadDeManos;
	
	public JugadorImpl(String nombre) {
		this.nombre = nombre;
		mano = new LinkedList<Carta>();
		resultado = null;
		saldo = 100;
		apuesta = 0;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Carta> getMano() {
		
		return this.mano;
	
	}

	public ResultadoMano resultado() {
	
		return this.resultado;
	
	}

	public void setResultado(ResultadoMano newResultado) {
	
		this.resultado = newResultado;
	
	}

	public int getSaldo() {
	
		return this.saldo;
	
	}

	public void setSaldo(int saldo) {
	
		this.saldo = saldo;
	
	}

	public int getApuesta() {
	
		return apuesta;
	
	}

	public void setApuesta(int apuesta) {
	
		this.apuesta = apuesta;
	
	}


	public int getCantidadDeManos() {
	
		return cantidadDeManos;
	
	}

	public void setCantidadDeManos(int cantidadDeManos) {
	
		this.cantidadDeManos = cantidadDeManos;
	
	}
	
}

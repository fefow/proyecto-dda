package dominio;

import java.rmi.RemoteException;
import java.util.Collections;
import java.util.LinkedList;

import proyecto.cei.comun.Carta;

public class Mazo {

	private LinkedList<Carta> mazo;
	
	private int numerodeCartas;

	public Mazo() {
		
		mazo = new LinkedList<Carta>();
		
		for (int tipo = 0; tipo < 4; tipo++) {
			
			for (int val = 1; val <= 13; val++) {
			
				mazo.add( new CartaImpl( val, TipoCarta.values()[tipo] ) );
			
			}
		}

		numerodeCartas = mazo.size();

	}

	// metodo para verificar nada mas
	
	public void MostrarMazo() throws RemoteException {
		
		for (Carta i : mazo) {
			
			System.out.println(i.toString());

		}
		
		System.out.println("Numero de cartas:" + numerodeCartas);
	
	}

	public void barajar() {
		
		Collections.shuffle(mazo);
	
	}

	public Carta darProximaCarta() {
	
		return mazo.pop();
	
	}

}

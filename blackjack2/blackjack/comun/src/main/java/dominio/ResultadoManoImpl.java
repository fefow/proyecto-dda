package dominio;

import proyecto.cei.comun.ResultadoMano;

public class ResultadoManoImpl implements ResultadoMano {

	private static final long serialVersionUID = 1L;
	
	private int resultado;
	
	private boolean esBJ;

	public ResultadoManoImpl() {
	
		this.resultado = -1;
		
		this.esBJ = false;
	
	}

	public int getResultado() {
	
		return this.resultado;
	
	}

	public void setResultado(int num) {
	
		this.resultado = num;
	
	}

	public void setEsBlackjack(Boolean e) {
	
		this.esBJ = e;
	
	}

	public boolean esBlackjack() {
	
		return this.esBJ;
	
	}

}

package proyecto.cei.comun;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Carta extends Remote, Serializable {
	public String getTipo() throws RemoteException;

	public int getValor() throws RemoteException;

	public String getFigura() throws RemoteException;
}
package proyecto.cei.comun;

import java.io.Serializable;
import java.util.List;;

public interface Jugador extends Serializable {
	
	public void setNombre(String nombre);

	public String getNombre();

	public void setId(String id);

	public String getId();

	public List<Carta> getMano();

	public ResultadoMano resultado();

	public void setResultado(ResultadoMano newMano);

	public int getSaldo();

	public void setSaldo(int saldo);

	public int getApuesta();

	public void setApuesta(int apuesta);


}

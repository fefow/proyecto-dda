package proyecto.cei.comun;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface ObservadorServidor extends Remote {

	public void notificarNuevoJugador(Jugador j) throws RemoteException;
	
	public void notificarJugadoresAnteriors(Jugador j, List<Jugador> jugadores) throws RemoteException;

	public void notificarCartaNueva(Jugador j) throws RemoteException;

	public void notificarTurnoJugador(Jugador jugadorTurno) throws RemoteException;

	public void notificarResultadoMano(Jugador j) throws RemoteException;

	public void notificarResultadoManoBlackjack(Jugador j) throws RemoteException;

	public void notificarManoCrupier(Jugador crupier) throws RemoteException;

	public void notificarRealizarApuesta(Jugador j) throws RemoteException;

	public void notificarBlackjack(Jugador j) throws RemoteException;

	public void notificarJugadorSaldo() throws RemoteException;

	public void notificarFinDeJuego() throws RemoteException;

	public void actualizarSaldo(Jugador j, int saldo) throws RemoteException;

}

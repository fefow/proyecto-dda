package proyecto.cei.comun;

import java.io.Serializable;

public interface ResultadoMano extends Serializable {
	public int getResultado();

	public void setResultado(int num);

	public void setEsBlackjack(Boolean e);

	public boolean esBlackjack();

}

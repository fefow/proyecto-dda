package proyecto.cei.server;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import proyecto.cei.comun.BlackjackServer;

/**
 * Hello world!
 *
 */
public class App 
{
	public static void main(String[] args) {
		System.setProperty("java.security.policy", "file:///d:/java.policy");
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}
		try {
			String name = "Compute";
			LocateRegistry.createRegistry(1099);
			BlackjackServerImpl server = new BlackjackServerImpl();
			BlackjackServer stub = (BlackjackServer) UnicastRemoteObject.exportObject(server, 0);
			Registry registry = LocateRegistry.getRegistry();
			registry.rebind(name, stub);
			System.out.println("Server on");
		} catch (Exception e) {
			System.err.println("ServerImpl exception:");
			e.printStackTrace();
		}
	}
}
